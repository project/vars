This module extends the Drupal core variable API that handles persistent variables.It allows finding the directory where a library has been installed (branch 2.x for Drupal 7).It also allows obtaining the value of a persistent variable without passing the default value to each function call and deletes multiple persistent variables.

INSTALLATION
============

Install and enable the module.Install the module only if you are instructed to do so, or you are a developer and want to use the module.If you are installing Variable API because it is a dependency of another module, you need to first install (and enable) Variable API, and then install (and enable) the other module. If the modules are enabled at the same time, you will get the error message class Vars not found. The same problem is actually present if you install a module that depends from the Variable API using Drush; in the moment I am writing this note, Drush will not download the Variable API module, with the consequence that you will get the error message class Vars not found.

ORIGINAL AUTHOR
===============
Module written by Git Migration
https://www.drupal.org/u/git-migration

MAINTAINER(S)
=============
2016: Vidhatanad https://www.drupal.org/u/vidhatanand
2016: Gaurav Kapoor https://www.drupal.org/u/gauravkapoor
